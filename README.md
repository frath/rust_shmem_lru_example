Example of how to use linux' shared memory between processes to establish a shared LRU cache behind a read-write lock.

_Requires nightly Rust (see `src/main.rs` for feature used)_

## Crates used:
* https://github.com/elast0ny/shared_memory for shared memory region management
* https://github.com/elast0ny/raw_sync-rs for a lock that can be placed into a shared memory region
* https://github.com/servo/uluru for an implementation of a LRU cache that contains all data, i.e., does not allocate
* https://github.com/nix-rust/nix for `fork()` and `wait()`
* https://github.com/rust-random/rand for some random example-data to fill into the LRU cache

## Example output:

``` shellsession
$ cargo run
[parent] shared memory created at: 0x7fd97c694000
[parent] LRU & RwLock established in shared memory
[child] shmem openend at: 0x7fd97c68f000
[child] lock acquired!
[child] LRU entry: None
[child] shmem openend at: 0x7fd97c68f000
[child] shmem openend at: 0x7fd97c68f000
[child] shmem openend at: 0x7fd97c68f000
[child] shmem openend at: 0x7fd97c68f000
[child] inserted 97 into the cache
[child] done
[child] lock acquired!
[child] LRU entry: Some(97)
[parent] child terminated
[child] inserted 3 into the cache
[child] done
[child] lock acquired!
[child] LRU entry: Some(3)
[parent] child terminated
[child] inserted 70 into the cache
[child] done
[child] lock acquired!
[child] LRU entry: Some(70)
[parent] child terminated
[child] inserted 72 into the cache
[child] done
[child] lock acquired!
[child] LRU entry: Some(72)
[parent] child terminated
[child] inserted 7 into the cache
[child] done
[parent] child terminated
[parent] all children terminated; done
```
