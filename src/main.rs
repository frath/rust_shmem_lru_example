#![feature(box_syntax)]

use std::mem::{forget, size_of};

use nix::sys::wait::wait;
use rand::Rng;
use raw_sync::locks::{LockInit, RwLock};
use shared_memory::ShmemConf;
use uluru::LRUCache;

// the cache we want to use; in this example, a cache that can hold (up to) 1000 u64 values
type OurLRUCache = LRUCache<u64, 1000>;

// size of our cache in bytes
static CACHE_SIZE: usize = size_of::<OurLRUCache>();

// in bytes, and large enough to safely contain an RwLock; size of an RwLock depends on its alignment, which is too annoying for this example to deal with
static SAFE_LOCK_SIZE: usize = 1024;

// name of our shared memory region, so the child processes can find it
static SHMEM_NAME: &'static str = "foobar_shared_memory";

fn parent_process() {
    let shmem = ShmemConf::new()
        .os_id(SHMEM_NAME)
        .size(SAFE_LOCK_SIZE + CACHE_SIZE)
        .create()
        .unwrap();

    println!("[parent] shared memory created at: {:?}", shmem.as_ptr());

    // construct LRU cache on the heap, so it can't overflow our stack, even if we make it big.
    // (requires nightly `box_syntax` feature)
    let boxed_lru = box OurLRUCache::default();

    // we will create the LRU cache behind the RwLock in our shared memory region, so leave some space
    let lru_ptr = unsafe { shmem.as_ptr().offset(SAFE_LOCK_SIZE as isize) };
    unsafe {
        // move the initialized LRU to our shared memory
        std::ptr::write(lru_ptr as *mut OurLRUCache, *boxed_lru);
    }

    // construct the RwLock at the beginning of the shared memory region
    let (lock, _) = unsafe { RwLock::new(shmem.as_ptr(), lru_ptr).unwrap() };

    println!("[parent] LRU & RwLock established in shared memory");

    for _ in 0..5 {
        match unsafe { nix::unistd::fork().unwrap() } {
            nix::unistd::ForkResult::Parent { .. } => (),
            nix::unistd::ForkResult::Child => {
                // forget values we don't want the child processes to drop..
                forget(shmem);
                forget(lock);
                return child_process();
            }
        }
    }

    while wait().is_ok() {
        println!("[parent] child terminated");
    }

    // just to signal that we need to hold lock until all child processes are terminated
    drop(lock);

    println!("[parent] all children terminated; done")
}

fn child_process() {
    let shmem = ShmemConf::new().os_id(SHMEM_NAME).open().unwrap();

    println!("[child] shmem openend at: {:?}", shmem.as_ptr());

    // offset at which we created the LRU cache
    let lru_ptr = unsafe { shmem.as_ptr().offset(SAFE_LOCK_SIZE as isize) };

    // re-create the RwLock, which is at the beginning of our shared memory region and which locks the address of the LRU cache
    let (lock, _) = unsafe { RwLock::from_existing(shmem.as_ptr(), lru_ptr).unwrap() };

    // acquire a (write) lock
    let lock_guard = lock.lock().unwrap();

    println!("[child] lock acquired!");

    let lru = unsafe { &mut *lock_guard.cast::<OurLRUCache>() };

    println!("[child] most recently used entry: {:?}", lru.front());
    let random_entry = rand::thread_rng().gen_range(0..100);
    lru.insert(random_entry);
    println!("[child] inserted {random_entry:?} into the cache");

    // explicitly drop the lock so we don't hold it longer than necessary
    drop(lock_guard);

    println!("[child] done")
}

fn main() {
    parent_process()
}
